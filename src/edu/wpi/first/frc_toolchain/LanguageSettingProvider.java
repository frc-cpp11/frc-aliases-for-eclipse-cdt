package edu.wpi.first.frc_toolchain;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.AbstractExecutableExtensionBase;
import org.eclipse.cdt.core.language.settings.providers.ILanguageSettingsProvider;
import org.eclipse.cdt.core.settings.model.CIncludePathEntry;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICLanguageSettingEntry;
import org.eclipse.cdt.core.settings.model.util.CDataUtil;
import org.eclipse.cdt.managedbuilder.core.BuildException;
import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.core.IManagedProject;
import org.eclipse.cdt.managedbuilder.core.IOption;
import org.eclipse.cdt.managedbuilder.core.IToolChain;
import org.eclipse.cdt.managedbuilder.core.ManagedBuildManager;
import org.eclipse.core.resources.IResource;

public class LanguageSettingProvider extends AbstractExecutableExtensionBase implements ILanguageSettingsProvider
{
	private Map<String, List<ICLanguageSettingEntry>> includePaths = new HashMap<String, List<ICLanguageSettingEntry>>();

	@Override
	public List<ICLanguageSettingEntry> getSettingEntries(ICConfigurationDescription cfgDescription, IResource rc, String languageId)
	{
		if (languageId == null || !languageId.startsWith("org.eclipse.cdt.core.g"))
		{
			return null;
		}
		List<ICLanguageSettingEntry> lst = new ArrayList<ICLanguageSettingEntry>();
		try
		{
			// Let us try this sketchy stuff
			IManagedProject proj = ManagedBuildManager.getBuildInfo(cfgDescription.getProjectDescription().getProject()).getManagedProject();
			for (IConfiguration i : proj.getConfigurations())
			{
				try
				{
					IToolChain tool = i.getToolChain();
					IOption prefix = tool.getOptionById("cdt.managedbuild.option.gnu.frc_aliases.prefix");
					IOption path = tool.getOptionById("cdt.managedbuild.option.gnu.frc_aliases.path");
					String command = languageId.substring(languageId.lastIndexOf('.') + 1);
					if (prefix.isValid())
					{
						command = prefix.getStringValue().concat(command);
					}
					if (path.isValid())
					{
						File testAbsolute = new File(path.getStringValue());
						if (testAbsolute.isDirectory())
						{
							testAbsolute = new File(testAbsolute, "bin");
							if (testAbsolute.isDirectory() && new File(testAbsolute, command).isFile())
							{
								command = testAbsolute.getCanonicalPath() + File.separator + command;
							}
							else if (new File(testAbsolute.getParentFile(), command).isFile())
							{
								command = testAbsolute.getParentFile().getCanonicalPath() + File.separator + command;
							}
						}
					}
					command = command.concat(" -print-prog-name=" + (languageId.endsWith("g++") ? "cc1plus" : "cc1"));
					List<ICLanguageSettingEntry> cache = includePaths.get(command);
					if (cache == null)
					{
						cache = new ArrayList<ICLanguageSettingEntry>();
						List<String> add = getIncludePaths(command);
						if (add != null)
						{
							for (String s : add)
							{
								// How about we check for WPILib ^.^
								File wpiTest = new File(s, "WPILib");
								if (wpiTest.isDirectory())
								{
									cache.add(CDataUtil.createCIncludePathEntry(wpiTest.getCanonicalPath(), CIncludePathEntry.INCLUDE_PATH));
								}
								cache.add(CDataUtil.createCIncludePathEntry(s, CIncludePathEntry.INCLUDE_PATH));
							}
						}
						includePaths.put(command, cache);
					}
					lst.addAll(cache);
				}
				catch (BuildException e)
				{
				}
				catch (IOException e)
				{
				}
			}
			return lst;
		}
		catch (NullPointerException e)
		{
			// Just assume that something was null and this isn't the right type
			// of project
			return null;
		}
	}

	private static List<String> getIncludePaths(String cmd) throws IOException
	{
		Process p = Runtime.getRuntime().exec(cmd);
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String cc1plusProgram = r.readLine();
		r.close();
		p.destroy();
		p = Runtime.getRuntime().exec(cc1plusProgram + " -v");
		r = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		List<String> includes = null;
		while (true)
		{
			String s = r.readLine();
			if (s == null)
			{
				break;
			}
			s = s.trim();
			if (s.equalsIgnoreCase("#include <...> search starts here:"))
			{
				includes = new ArrayList<String>();
			}
			else if (s.equalsIgnoreCase("end of search list."))
			{
				break;
			}
			else if (includes != null)
			{
				File path = new File(s);
				if (path.isDirectory())
				{
					includes.add(path.getCanonicalPath());
				}
			}
		}
		// Bonus include paths -- these are just special cases that gcc doesn't
		// seem to catch
		p.getInputStream().close();
		p.destroy();
		return includes;
	}
}
