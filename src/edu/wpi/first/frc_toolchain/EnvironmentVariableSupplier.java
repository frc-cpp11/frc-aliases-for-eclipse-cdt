package edu.wpi.first.frc_toolchain;

import java.io.File;

import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.core.IOption;
import org.eclipse.cdt.managedbuilder.envvar.IBuildEnvironmentVariable;
import org.eclipse.cdt.managedbuilder.envvar.IConfigurationEnvironmentVariableSupplier;
import org.eclipse.cdt.managedbuilder.envvar.IEnvironmentVariableProvider;
import org.eclipse.core.runtime.Platform;

public class EnvironmentVariableSupplier implements IConfigurationEnvironmentVariableSupplier
{
	public IBuildEnvironmentVariable getVariable(String variableName, IConfiguration configuration, IEnvironmentVariableProvider provider)
	{
		if (PathEnvironmentVariable.isVar(variableName))
		{
			return PathEnvironmentVariable.create(configuration);
		}
		return null;
	}

	public IBuildEnvironmentVariable[] getVariables(IConfiguration configuration, IEnvironmentVariableProvider provider)
	{
		IBuildEnvironmentVariable path = PathEnvironmentVariable.create(configuration);
		return path != null ? new IBuildEnvironmentVariable[] { path } : new IBuildEnvironmentVariable[0];
	}

	private static class PathEnvironmentVariable implements IBuildEnvironmentVariable
	{
		public static String name = "PATH";
		private File path;

		private PathEnvironmentVariable(File path)
		{
			this.path = path;
		}

		public static PathEnvironmentVariable create(IConfiguration configuration)
		{
			IOption prefix = configuration.getToolChain().getOptionById("cdt.managedbuild.option.gnu.frc_aliases.prefix");
			File sysroot = new File(prefix.getValue().toString());
			File bin = new File(sysroot, "bin");
			if (bin.isDirectory())
				sysroot = bin;
			return new PathEnvironmentVariable(sysroot);
		}

		public static boolean isVar(String name)
		{
			return Platform.getOS().equals("win32") ? name.equalsIgnoreCase(name) : name.equals(name);
		}

		public String getDelimiter()
		{
			return Platform.getOS().equals("win32") ? ";" : ":";
		}

		public String getName()
		{
			return name;
		}

		public int getOperation()
		{
			return 3;
		}

		public String getValue()
		{
			return this.path.getAbsolutePath();
		}
	}
}
