package edu.wpi.first.frc_toolchain;

import org.eclipse.cdt.managedbuilder.core.IBuildObject;
import org.eclipse.cdt.managedbuilder.core.IManagedCommandLineGenerator;
import org.eclipse.cdt.managedbuilder.core.IManagedCommandLineInfo;
import org.eclipse.cdt.managedbuilder.core.IOption;
import org.eclipse.cdt.managedbuilder.core.ITool;
import org.eclipse.cdt.managedbuilder.core.IToolChain;
import org.eclipse.cdt.managedbuilder.internal.core.ResourceConfiguration;

public class CommandLineGenerator implements IManagedCommandLineGenerator
{
	public static final String CMD_LINE_PRM_NAME = "COMMAND";
	public static final String FLAGS_PRM_NAME = "FLAGS";
	public static final String OUTPUT_FLAG_PRM_NAME = "OUTPUT_FLAG";
	public static final String OUTPUT_PREFIX_PRM_NAME = "OUTPUT_PREFIX";
	public static final String OUTPUT_PRM_NAME = "OUTPUT";
	public static final String INPUTS_PRM_NAME = "INPUTS";

	private static String makeVariable(String variableName)
	{
		return "${" + variableName + "}";
	}

	@Override
	public IManagedCommandLineInfo generateCommandLineInfo(ITool tool, String commandName, String[] flags, String outputFlag, String outputPrefix,
			String outputName, String[] inputResources, String commandLinePattern)
	{
		IBuildObject parent = tool.getParent();
		IToolChain toolchain;
		if ((parent instanceof ResourceConfiguration))
		{
			toolchain = ((ResourceConfiguration) parent).getBaseToolChain();
		}
		else
		{
			toolchain = (IToolChain) parent;
		}
		IOption prefix = toolchain.getOptionById("cdt.managedbuild.option.gnu.frc_aliases.prefix");

		if (commandLinePattern == null || commandLinePattern.length() <= 0)
		{
			commandLinePattern = "${COMMAND} ${FLAGS} ${OUTPUT_FLAG} ${OUTPUT_PREFIX}${OUTPUT} ${INPUTS}";
		}

		if (outputName.length() > 0 && outputName.indexOf("$(") != 0)
			outputName = '"' + outputName + '"';

		String inputsStr = "";
		if (inputResources != null)
		{
			for (String inp : inputResources)
			{
				if (inp != null && inp.length() > 0)
				{
					if (inp.indexOf("$(") != 0)
					{
						inp = '"' + inp + '"';
					}
					inputsStr = inputsStr + inp + ' ';
				}
			}
			inputsStr = inputsStr.trim();
		}

		String flagsStr = stringArrayToString(flags);

		String command = commandLinePattern;

		command = command.replace(makeVariable(CMD_LINE_PRM_NAME.toLowerCase()), prefix.getValue().toString() + commandName);
		command = command.replace(makeVariable(FLAGS_PRM_NAME.toLowerCase()), flagsStr);
		command = command.replace(makeVariable(OUTPUT_FLAG_PRM_NAME.toLowerCase()), outputFlag);
		command = command.replace(makeVariable(OUTPUT_PREFIX_PRM_NAME.toLowerCase()), outputPrefix);
		command = command.replace(makeVariable(OUTPUT_PRM_NAME.toLowerCase()), outputName);
		command = command.replace(makeVariable(INPUTS_PRM_NAME.toLowerCase()), inputsStr);

		command = command.replace(makeVariable(CMD_LINE_PRM_NAME), prefix.getValue().toString() + commandName);
		command = command.replace(makeVariable(FLAGS_PRM_NAME), flagsStr);
		command = command.replace(makeVariable(OUTPUT_FLAG_PRM_NAME), outputFlag);
		command = command.replace(makeVariable(OUTPUT_PREFIX_PRM_NAME), outputPrefix);
		command = command.replace(makeVariable(OUTPUT_PRM_NAME), outputName);
		command = command.replace(makeVariable(INPUTS_PRM_NAME), inputsStr);

		return new CommandLineInfo(command.trim(), commandLinePattern, commandName, stringArrayToString(flags), outputFlag, outputPrefix, outputName,
				stringArrayToString(inputResources));
	}

	private String stringArrayToString(String[] array)
	{
		if (array == null || array.length <= 0)
			return new String();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < array.length; i++)
			sb.append(array[i] + ' ');
		return sb.toString().trim();
	}
}
