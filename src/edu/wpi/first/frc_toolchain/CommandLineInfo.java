package edu.wpi.first.frc_toolchain;

import org.eclipse.cdt.managedbuilder.core.IManagedCommandLineInfo;

public class CommandLineInfo implements IManagedCommandLineInfo {
	private String commandLine;
	private String commandLinePattern;
	private String commandName;
	private String flags;
	private String outputFlag;
	private String outputPrefix;
	private String outputName;
	private String inputResources;

	public CommandLineInfo(String commandLine, String commandLinePattern,
			String commandName, String flags, String outputFlag,
			String outputPrefix, String outputName, String inputResources) {
		this.commandLine = commandLine;
		this.commandLinePattern = commandLinePattern;
		this.commandName = commandName;
		this.flags = flags;
		this.outputFlag = outputFlag;
		this.outputPrefix = outputPrefix;
		this.outputName = outputName;
		this.inputResources = inputResources;
	}

	public String getCommandLine() {
		return this.commandLine;
	}

	public String getCommandLinePattern() {
		return this.commandLinePattern;
	}

	public String getCommandName() {
		return this.commandName;
	}

	public String getFlags() {
		return this.flags;
	}

	public String getOutputFlag() {
		return this.outputFlag;
	}

	public String getOutputPrefix() {
		return this.outputPrefix;
	}

	public String getOutput() {
		return this.outputName;
	}

	public String getInputs() {
		return this.inputResources;
	}
}
