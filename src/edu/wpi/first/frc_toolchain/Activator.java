package edu.wpi.first.frc_toolchain;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;

public class Activator extends Plugin
{
	public static final String PLUGIN_ID = "edu.wpi.first.frc-toolchain";
	private static Activator plugin;

	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		plugin = this;
		getDefault().setDebugging(true);
	}

	public void stop(BundleContext context) throws Exception
	{
		plugin = null;
		super.stop(context);
	}

	public static void log(Throwable e)
	{
		getDefault().getLog().log(new Status(4, PLUGIN_ID, 1, "Internal Error", e));
	}

	public static Activator getDefault()
	{
		return plugin;
	}
}
